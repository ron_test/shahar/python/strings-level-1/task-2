def insert_string_middle(base_string, insert_string):
    middle_index = len(base_string) // 2
    result_string = base_string[:middle_index] + \
        insert_string + base_string[middle_index:]
    return result_string


base_string = input("Enter the base string: ")
insert_string = input("Enter the string to insert: ")

result = insert_string_middle(base_string, insert_string)
print(result)
